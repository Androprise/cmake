#include "cmExtraVSCodeGenerator.h"

#include <map>
#include <ostream>
#include <set>
#include <utility>

#include "cmAlgorithms.h"
#include "cmGeneratedFileStream.h"
#include "cmGeneratorTarget.h"
#include "cmGlobalGenerator.h"
#include "cmLocalGenerator.h"
#include "cmMakefile.h"
#include "cmSourceFile.h"
#include "cmStateTypes.h"
#include "cmSystemTools.h"
#include "cmXMLWriter.h"
#include "cmake.h"
#include "json.hpp"

json::JSON taskArray; // A JSON array to hold each task
json::JSON includePathArray;
json::JSON definesArray;

/*
 * See https://code.visualstudio.com/docs/languages/cpp for files and format
 * info.
 */

cmExtraVSCodeGenerator::cmExtraVSCodeGenerator()
  : cmExternalMakefileProjectGenerator()
{
  taskArray = json::Array();
}

void cmExtraVSCodeGenerator::Generate()
{

  // for each sub project in the project create Visual Studio code task and
  // properties json files
  for (auto const& it : this->GlobalGenerator->GetProjectMap()) {
    // create a project file
    this->CreateProjectFiles(it.second);
  }
}

cmExternalMakefileProjectGeneratorFactory* cmExtraVSCodeGenerator::GetFactory()
{
  static cmExternalMakefileProjectGeneratorSimpleFactory<
    cmExtraVSCodeGenerator>
    factory("VSCode",
            "Generates Visual Studio Code task and properties files.");

  if (factory.GetSupportedGlobalGenerators().empty()) {
#if defined(_WIN32)
    factory.AddSupportedGlobalGenerator("MinGW Makefiles");
    factory.AddSupportedGlobalGenerator("NMake Makefiles");
    factory.AddSupportedGlobalGenerator("NMake Makefiles JOM");
// disable until somebody actually tests it:
// this->AddSupportedGlobalGenerator("MSYS Makefiles");
#endif
    factory.AddSupportedGlobalGenerator("Ninja");
    factory.AddSupportedGlobalGenerator("Unix Makefiles");
  }

  return &factory;
}

void cmExtraVSCodeGenerator::CreateProjectFiles(
  const std::vector<cmLocalGenerator*>& lgs)
{
  if (lgs[0]->IsRootMakefile()) {
    taskFilename =
      std::string(lgs[0]->GetSourceDirectory()) + "/.vscode/tasks.json";
    propFilename =
      lgs[0]->GetSourceDirectory() + "/.vscode/c_cpp_properties.json";
  }
  const cmMakefile* mf = lgs[0]->GetMakefile();
  std::string make = mf->GetRequiredDefinition("CMAKE_MAKE_PROGRAM");
  const std::string makeArgs =
    mf->GetSafeDefinition("CMAKE_CODEBLOCKS_MAKE_ARGUMENTS");
  AddTasksAndProperties("all", nullptr, make.c_str(), lgs[0], makeArgs, false);
  AddTasksAndProperties("clean", nullptr, make.c_str(), lgs[0], makeArgs,
                        false);
  // add all executable and library targets and some of the GLOBAL
  // and UTILITY targets
  for (cmLocalGenerator* lg : lgs) {
    const std::vector<cmGeneratorTarget*>& targets = lg->GetGeneratorTargets();
    for (cmGeneratorTarget* target : targets) {
      std::string targetName = target->GetName();
      switch (target->GetType()) {
        case cmStateEnums::GLOBAL_TARGET: {
          // Only add the global targets from CMAKE_BINARY_DIR,
          // not from the subdirs
          if (lg->GetCurrentBinaryDirectory() == lg->GetBinaryDirectory()) {
            this->AddTasksAndProperties(targetName, nullptr, make.c_str(), lg,
                                        makeArgs, false);
          }
        } break;
        case cmStateEnums::UTILITY:
          // Add all utility targets, except the Nightly/Continuous/
          // Experimental-"sub"targets as e.g. NightlyStart
          if (((targetName.find("Nightly") == 0) &&
               (targetName != "Nightly")) ||
              ((targetName.find("Continuous") == 0) &&
               (targetName != "Continuous")) ||
              ((targetName.find("Experimental") == 0) &&
               (targetName != "Experimental"))) {
            break;
          }

          this->AddTasksAndProperties(targetName, nullptr, make.c_str(), lg,
                                      makeArgs, false);
          break;
        case cmStateEnums::EXECUTABLE:
        case cmStateEnums::STATIC_LIBRARY:
        case cmStateEnums::SHARED_LIBRARY:
        case cmStateEnums::MODULE_LIBRARY:
        case cmStateEnums::OBJECT_LIBRARY: {
          cmGeneratorTarget* gt = target;
          this->AddTasksAndProperties(targetName, gt, make.c_str(), lg,
                                      makeArgs, true);
          std::string fastTarget = targetName;
          fastTarget += "/fast";
          this->AddTasksAndProperties(fastTarget, gt, make.c_str(), lg,
                                      makeArgs, false);
        } break;
        default:
          break;
      }
    }
  }
  std::string cStandard = mf->GetSafeDefinition("CMAKE_C_STANDARD");
  std::string cppStandard = mf->GetSafeDefinition("CMAKE_CXX_STANDARD");
  if (cStandard.empty())
    cStandard = "${default}";
  else
    cStandard = "c" + cStandard;
  if (cppStandard.empty())
    cppStandard = "${default}";
  else
    cppStandard = "c++" + cppStandard;

  json::JSON taskJson = { "version", "2.0.0", "tasks", taskArray };
  json::JSON propertiesJson = {
    "name", "CMake", "includePath", includePathArray, "defines", definesArray,
    "compilerPath", GetCompiler(mf), "intelliSenseMode",
    "clang-x64", // Current options are msvc-x64 and clang-x64. makefiles do
                 // not use MSVC
    "cppStandard", cppStandard, "cStandard", cStandard
  };
  json::JSON fullPropJson = { "version", 4, "configurations",
                              json::Array(propertiesJson) };
  std::ofstream outFile(taskFilename);
  if (outFile) {
    outFile << taskJson << std::endl;
  }
  outFile.close();
  std::ofstream outFileProp(propFilename);
  if (outFileProp) {
    outFileProp << fullPropJson << std::endl;
  }
  outFileProp.close();
}

/**
 * Add the tasks to the tasks.json file in {PROJECT_ROOT}/.vscode and add
 * includes and defines to each source dir's .vscode/c_cpp_properties.json file
 */
void cmExtraVSCodeGenerator::AddTasksAndProperties(
  const std::string& targetName, cmGeneratorTarget* target, const char* make,
  const cmLocalGenerator* lg, const std::string& makeFlags,
  const bool& needsCleanTask)
{
  cmMakefile const* makefile = lg->GetMakefile();
  std::string makefileName = lg->GetCurrentBinaryDirectory();
  makefileName += "/Makefile";

  // First add the target build and clean tasks to the tasks.json file

  std::string workingDir = lg->GetCurrentBinaryDirectory();
  if (target != nullptr) {
    if (target->GetType() == cmStateEnums::EXECUTABLE) {
      // Determine the directory where the executable target is created, and
      // set the working directory to this dir.
      const char* runtimeOutputDir =
        makefile->GetDefinition("CMAKE_RUNTIME_OUTPUT_DIRECTORY");
      if (runtimeOutputDir != nullptr) {
        workingDir = runtimeOutputDir;
      } else {
        const char* executableOutputDir =
          makefile->GetDefinition("EXECUTABLE_OUTPUT_PATH");
        if (executableOutputDir != nullptr) {
          workingDir = executableOutputDir;
        }
      }
    }
  } else {
    workingDir = lg->GetCurrentBinaryDirectory();
  }
  // Create a task to build this target
  json::JSON flagsArray = json::Array();
  flagsArray.append("-f");
  flagsArray.append("\"" + makefileName + "\"");
  StringSplit(makeFlags, flagsArray);
  flagsArray.append(targetName);

  std::string taskName = targetName;
  if (needsCleanTask) {
    // Append "- build" to the build task name
    taskName += " - build";
  }
  json::JSON buildTask = {
    "label",   taskName,   "type",    "shell",
    "command", make,       "options", { "cwd", workingDir },
    "args",    flagsArray, "group",   "build"
  };
  taskArray.append(buildTask);

  // Create a task to clean this target if it is not a 'fast' variant
  // (targetname/fast) version of the target
  if (needsCleanTask) {
    json::JSON cleanFlagsArray = json::Array();
    cleanFlagsArray.append("-f");
    cleanFlagsArray.append("\"" + makefileName + "\"");
    StringSplit(makeFlags, cleanFlagsArray);
    cleanFlagsArray.append("clean");
    json::JSON cleanTask = { "label",   targetName + " - clean",
                             "type",    "shell",
                             "command", make,
                             "options", { "cwd", workingDir },
                             "args",    cleanFlagsArray,
                             "group",   "build" };
    taskArray.append(cleanTask);
  }

  // Next add the includes and defines for the c_cpp_properties.json file

  if (target != nullptr) {
    std::string buildType = makefile->GetSafeDefinition("CMAKE_BUILD_TYPE");
    std::vector<std::string> cdefs;
    target->GetCompileDefinitions(cdefs, buildType, "C");

    // Add include paths for Intellisense
    includePathArray.append("${workspaceFolder}");
    for (std::string const& d : cdefs) {
      definesArray.append(d);
    }

    // Add defines for Intellisense
    std::vector<std::string> allIncludeDirs;

    std::vector<std::string> includes;
    lg->GetIncludeDirectories(includes, target, "C", buildType);

    allIncludeDirs.insert(allIncludeDirs.end(), includes.begin(),
                          includes.end());

    std::string systemIncludeDirs = makefile->GetSafeDefinition(
      "CMAKE_EXTRA_GENERATOR_CXX_SYSTEM_INCLUDE_DIRS");
    if (!systemIncludeDirs.empty()) {
      std::vector<std::string> dirs;
      cmSystemTools::ExpandListArgument(systemIncludeDirs, dirs);
      allIncludeDirs.insert(allIncludeDirs.end(), dirs.begin(), dirs.end());
    }

    systemIncludeDirs = makefile->GetSafeDefinition(
      "CMAKE_EXTRA_GENERATOR_C_SYSTEM_INCLUDE_DIRS");
    if (!systemIncludeDirs.empty()) {
      std::vector<std::string> dirs;
      cmSystemTools::ExpandListArgument(systemIncludeDirs, dirs);
      allIncludeDirs.insert(allIncludeDirs.end(), dirs.begin(), dirs.end());
    }

    std::vector<std::string>::const_iterator end =
      cmRemoveDuplicates(allIncludeDirs);

    for (std::vector<std::string>::const_iterator i = allIncludeDirs.begin();
         i != end; ++i) {
      includePathArray.append(*i);
    }
  }
}

bool cmExtraVSCodeGenerator::StringEndsWith(std::string const& fullString,
                                            std::string const& ending)
{
  if (fullString.length() >= ending.length()) {
    return (0 ==
            fullString.compare(fullString.length() - ending.length(),
                               ending.length(), ending));
  } else {
    return false;
  }
}

void cmExtraVSCodeGenerator::StringSplit(const std::string& str,
                                         json::JSON& arrayContainer,
                                         char delim)
{
  std::stringstream ss(str);
  std::string token;
  while (std::getline(ss, token, delim)) {
    arrayContainer.append(token);
  }
}

std::string cmExtraVSCodeGenerator::GetCompiler(const cmMakefile* mf)
{
  // figure out which language to use
  // for now care only for C, C++, and Fortran

  // projects with C/C++ and Fortran are handled as C/C++ projects
  std::string compilerVar;
  if (this->GlobalGenerator->GetLanguageEnabled("CXX")) {
    compilerVar = "CMAKE_CXX_COMPILER";
  } else if (this->GlobalGenerator->GetLanguageEnabled("C")) {
    compilerVar = "CMAKE_C_COMPILER";
  } else if (this->GlobalGenerator->GetLanguageEnabled("Fortran")) {
    compilerVar = "CMAKE_Fortran_COMPILER";
  }

  std::string compiler = mf->GetSafeDefinition(compilerVar);
  return compiler;
}
