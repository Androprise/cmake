/* Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
   file Copyright.txt or https://cmake.org/licensing for details.  */
#ifndef cmExtraVSCodeGenerator_h
#define cmExtraVSCodeGenerator_h

#include "cmConfigure.h" // IWYU pragma: keep

#include "cmExternalMakefileProjectGenerator.h"

#include <string>
#include <vector>

namespace json {
class JSON;
}

class cmGeneratorTarget;
class cmLocalGenerator;
class cmMakefile;

/** \class cmExtraVsCodeGenerator
 * \brief Write Visual Studio Code files for Makefile based projects
 */
class cmExtraVSCodeGenerator : public cmExternalMakefileProjectGenerator
{
public:
  cmExtraVSCodeGenerator();

  static cmExternalMakefileProjectGeneratorFactory* GetFactory();

  void Generate() override;

private:
  std::string taskFilename;
  std::string propFilename;

  void CreateProjectFiles(const std::vector<cmLocalGenerator*>& lgs);

  std::string BuildMakeCommand(const std::string& make, const char* makefile,
                               const std::string& target,
                               const std::string& makeFlags);
  void AddTasksAndProperties(const std::string& targetName,
                             cmGeneratorTarget* target, const char* make,
                             const cmLocalGenerator* lg,
                             const std::string& makeFlags,
                             const bool& needsCleanTask);
  void StringSplit(const std::string& str, json::JSON& arrayContainer,
                   char delim = ' ');
  std::string GetCompiler(const cmMakefile* mf);
  bool StringEndsWith(std::string const& fullString,
                      std::string const& ending);
};

#endif
